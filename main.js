(function(){

	//col,rowは奇数でなくてはいけない
	var Maze = function(col,row){
		this.map = [];

		this.col = col;
		this.row = row;

		//棒の倒し先を管理する配列
		this.points = [
			//上に倒す
			[0,1],
			//右に倒す
			[1,0],
			//下に倒す
			[0,-1],
			//左に倒す
			[-1,0]
		];

		this.startX = 0;
		this.startY = 0;
		this.goalX = col - 1;
		this.goalY = row - 1;

		this.init = function(){
			//全てのmap配列を0で埋める (col * row)
			for(var x = 0;x < col; x++){
				//配列の中に配列を作る
				this.map[x] = [];
				for(var y = 0; y < row; y++){
					this.map[x][y] = 0;
				}
			}

			//一つ飛びに1(壁)を入れる
			for(var x = 1; x < col; x += 2){
				for(var y = 1; y < row; y += 2){
					this.map[x][y] = 1;
				}
			}

			//棒を倒して迷路を作る配列にする
			for(var x = 1; x < col; x += 2){
				for(var y = 1; y < row; y += 2){

					do{
						//x座標が1のの壁(1つおきに設置したやつだった場合)
						if(x === 1){
							//配列pointsの中4パターンの中からランダムに選択
							var r = this.points[this.rand(3)];
						}else{
						//それ以外の設置した壁の場合		左に移動以外のpointsの中のパターンを探す。
							var r = this.points[this.rand(2)];
						}
						//壁同士が重なったらもう一度pointsのパターンの中から選びなおす。
					}while(this.map[x + r[0]][y + r[1]] === 1);

					//移動した先も壁に(1に)する。
					this.map[x + r[0]][y + r[1]] = 1;
				}
			}
		}//init()

		//乱数を返すメソッド
		this.rand = function(n){
			return Math.floor(Math.random() * (n + 1));
		}

		//描画するメソッド
		this.draw = function(){
			var view = new View();
			view.draw(this);
		};
	};

	var View = function(){

		//壁のサイズを10pxに設定する
		this.wallSize = 10;
		this.wallColor = "#3261ab";
		this.routeColor = "#eeccdd";

		this.canvas = document.getElementById("mycanvas");
		if(!this.canvas || !this.canvas.getContext) return false;
		this.ctx = this.canvas.getContext("2d");


		this.draw = function(maze){

			//壁のサイズ分2を上下左右に足す
			this.canvas.width  = (maze.col + 2) * this.wallSize;
		    this.canvas.height = (maze.row + 2) * this.wallSize;

			//上下の壁
			for(var x = 0;x < maze.col + 2; x++){
				this.drawWall(x,0);
				this.drawWall(x,maze.row + 1);
			}

			//左右の壁
			for(var y = 0;y < maze.row + 2;y++){
				this.drawWall(0,y);
				this.drawWall(maze.col + 1,y);
			}

			//中身
			for(var x = 0;x < maze.col + 2;x++){
				for(var y = 0;y < maze.row + 2;y++){
					if(maze.map[x][y] === 1){
						this.drawWall(x + 1,y + 1);
					}
					if((x === maze.startX && y === maze.startY) || (x === maze.goalX && y === maze.goalY)){
						this.drawRoute(x + 1,y + 1);
					}
				}
			}
		};

		//描画する関数
		this.drawWall = function(x,y){
			this.ctx.fillStyle = this.wallColor;
			this.drawRect(x,y)
		}

		//ルートを描画する関数
		this.drawRoute = function(x,y){
			this.ctx.fillStyle = this.routeColor;
			this.drawRect(x,y)
		}

		//描画する関数(共通)
		this.drawRect = function(x,y){
			this.ctx.fillRect(x * this.wallSize,y * this.wallSize,this.wallSize,this.wallSize);
		}

	};//new View

	function reset(){
		var maze = new Maze(101,13);
		maze.init();
		maze.draw();
	}

	//reset();

	document.getElementById("reset-btn").addEventListener('click',reset,false);

	/*
		0が道、1が壁として描画する

		0,0,0
		0,1,0
		0,1,0
	*/

})();